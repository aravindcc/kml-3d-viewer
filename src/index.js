import * as THREE from "three";
import { OrbitControls } from "three-full";
import { Geometry } from "three";
import Worker from "worker-loader!./worker";

/* global THREE */
let scene;
let camera;
let renderer;

// hold onto the main mesh
var mesh;
var geoqueue = [];
var receivedCount = 0;
var oldFrontier = 0;
var cachedFrontier = 0;

const length = 50000000;
const size = Float32Array.BYTES_PER_ELEMENT * length;

// shared buffer between worker to transfer verticies.
const sharedVertBuffer = new SharedArrayBuffer(size);
const sharedVertArray = new Float32Array(sharedVertBuffer);
const sharedNormBuffer = new SharedArrayBuffer(size);
const sharedNormArray = new Float32Array(sharedNormBuffer);

var worker = new Worker();

function init() {
  // renderer
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x404040, 1);
  document.body.appendChild(renderer.domElement);

  // scene
  scene = new THREE.Scene();

  // camera
  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );
  camera.position.set(3, 3, 3);

  // controls
  let controls = new OrbitControls(camera, renderer.domElement);

  // light
  var light = new THREE.PointLight(0xff0000, 2);
  light.position.set(-2, 2, 2);
  scene.add(light);

  // add mesh
  var meshMaterial = new THREE.MeshLambertMaterial({
    color: 0xffffff,
    opacity: 1,
    transparent: false
  });
  var meshGeo = new THREE.BufferGeometry();
  mesh = new THREE.Mesh(meshGeo, meshMaterial);
  scene.add(mesh);

  // start worker
  worker.postMessage({ buff: sharedVertBuffer, type: "vert" });
  worker.postMessage({ buff: sharedNormBuffer, type: "norm" });
  worker.postMessage({ type: "iterate" });
  animate();
}

function recache() {
  // update the GPU with the new verticies.
  let newFrontier = geoqueue[0];
  if (cachedFrontier < newFrontier) {
    var t0 = performance.now();
    mesh.geometry.setAttribute(
      "position",
      new THREE.Float32BufferAttribute(sharedVertArray, 3)
    );
    mesh.geometry.setAttribute(
      "normal",
      new THREE.Float32BufferAttribute(sharedNormArray, 3)
    );
    cachedFrontier = geoqueue[Math.floor(geoqueue.length / 2)];
    var t1 = performance.now();
    console.log("Cache took " + (t1 - t0) + " milliseconds.");
  }
}

function animate() {
  if (geoqueue.length > 0) {
    let newFrontier = geoqueue[0];
    document.getElementById("status").innerHTML = "animatingTo: " + newFrontier;
    recache();
    oldFrontier += 10000;
    oldFrontier = Math.min(oldFrontier, newFrontier);
    if (oldFrontier == newFrontier) {
      geoqueue.shift();
      document.getElementById("counter").innerHTML =
        "# frames queued: " + geoqueue.length;
    }
    mesh.geometry.setDrawRange(0, oldFrontier);
  }
  renderer.render(scene, camera);
  setTimeout(function() {
    requestAnimationFrame(animate);
  }, 40);
}

// start
window.addEventListener("load", () => {
  worker.onmessage = e => {
    if (e.data.type == "start") {
      document.body.removeChild(document.getElementById("loading"));
      init(); // init threejs
    } else {
      // a new set of verticies has been added (this doesn't update the verticies recache does.)
      geoqueue.push(e.data.verts);
      worker.postMessage({ type: "iterate" });

      document.getElementById("counter").innerHTML =
        "# frames queued: " + geoqueue.length;

      document.getElementById("received").innerHTML =
        "# frames received: " + receivedCount;

      receivedCount += 1;
    }
  };
  var f = document.createElement("input");
  f.type = "file";
  f.name = "file";
  f.id = "file";
  document.body.appendChild(f);
  f.onchange = function() {
    worker.postMessage({ type: "start", file: this.files[0] });
    document.body.removeChild(f);
    var a = document.createElement("a");
    a.innerHTML = "loading ...";
    a.id = "loading";
    document.body.appendChild(a);
  };
});
