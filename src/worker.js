// a background queue of array of points.
// these will put into the main queue sets of convexgeometry objects.
// see speeding up mesh data three.js
// then post onto main queue that will begin consuming after 10 and above, and shows in animation count.
import * as THREE from "three";

// this is for fake generation
const SINGLE_ITER_DIST = 0.01;
const SINGLE_STEP = 0.01;
const SINGLE_ANGLE_STEP = 0.01;
const SINGLE_MAX_DIMPLE = 0.01;

function round(value, step) {
  step || (step = 1.0);
  var inv = 1.0 / step;
  return Math.round(value * inv) / inv;
}

function sortPoints(points) {
  var startAng;
  var uniquePoints = {};
  points.forEach(point => {
    var ang = round(Math.atan2(point.z, point.y), 0.004);
    if (!startAng) {
      startAng = ang;
    } else if (ang < startAng) {
      // ensure that all points are clockwise of the start point
      ang += Math.PI * 2;
    }
    point.angle = ang; // add the angle to the point
    uniquePoints[ang] = point;
  });

  // Sort clockwise;
  points = Object.values(uniquePoints);
  points.sort((a, b) => a.angle - b.angle);
  return points;
}

// fake data generation
function genRoughCircle(radius, x) {
  var points = [];
  for (var i = 0; i < Math.PI * 2; i += SINGLE_ANGLE_STEP) {
    var y =
      Math.cos(i) * radius +
      Math.random() * radius * SINGLE_MAX_DIMPLE * Math.cos(i);
    var z =
      Math.sin(i) * radius +
      Math.random() * radius * SINGLE_MAX_DIMPLE * Math.cos(i);
    points.push(new THREE.Vector3(x, y, z));
  }

  var startAng;
  points.forEach(point => {
    var ang = Math.atan2(point.z, point.y);
    if (!startAng) {
      startAng = ang;
    } else if (ang < startAng) {
      // ensure that all points are clockwise of the start point
      ang += Math.PI * 2;
    }
    point.angle = ang; // add the angle to the point
  });

  // Sort clockwise;
  points.sort((a, b) => a.angle - b.angle);

  return points;
}

function genRoughPointCloud(offset = 0) {
  var points = [];
  for (var i = 0; i < SINGLE_ITER_DIST / SINGLE_STEP; i++) {
    points[i] = genRoughCircle(1, i * SINGLE_STEP + offset);
  }
  return {
    points,
    dist: SINGLE_ITER_DIST + offset,
    diff: SINGLE_ITER_DIST
  };
}

// this to get the face and the reverse face from three points
const reversi = (set, a, b, c) => {
  var tri1 = new THREE.Triangle();
  var tri2 = new THREE.Triangle();
  tri1.set(a, b, c);
  tri2.set(c, b, a);
  set.push(tri1);
  set.push(tri2);
};

// this will get the faces from the points
// then return a flatten list of vertices (of the faces) and corresponding nromals
const getVertsAndNormals = (points, shift = 0) => {
  var triangles = [];
  var faces = [];
  var vertices = [];
  var normals = [];

  for (var j = 0; j < points.length - 1; j++) {
    for (var i = 0; i < Math.max(points[j].length, points[j + 1].length); i++) {
      var a1 = points[j][i % points[j].length];
      var b1 = points[j][(i + 1) % points[j].length];
      var c1 = points[j + 1][i % points[j + 1].length];

      var a2 = points[j][(i + 1) % points[j].length];
      var b2 = points[j + 1][i % points[j + 1].length];
      var c2 = points[j + 1][(i + 1) % points[j + 1].length];

      reversi(triangles, a1, b1, c1);
      reversi(triangles, a2, b2, c2);
    }
  }

  for (var i = 0; i < triangles.length; i++) {
    var triangle = triangles[i];
    var normal = new THREE.Vector3();
    triangle.getNormal(normal);

    [triangle.a, triangle.b, triangle.c].forEach(point => {
      // faces.push(new THREE.Face3(point.x - shift, point.y, point.z, normal));
      vertices.push(point.x - shift, point.y, point.z);
      normals.push(normal.x, normal.y, normal.z);
    });
  }

  return { vertices, normals };
};

// globals
var sharedVertArray;
var sharedNormArray;
var refDist = 0;
var globalI = 0;
var storedPoints = {};
var storedKeys = [];

// interacting with main queue
self.addEventListener("message", event => {
  if (event.data.type == "vert") {
    sharedVertArray = new Uint32Array(event.data.buff);
  } else if (event.data.type == "norm") {
    sharedNormArray = new Uint32Array(event.data.buff);
  } else if (event.data.type == "iterate") {
    var t0 = performance.now();
    iterate(); // <---- The function you're measuring time for
    var t1 = performance.now();
    console.log("Iteration took " + (t1 - t0) + " milliseconds.");
  } else {
    // start loading file
    // TODO: make it as a stream but more needed when live data.
    var file = event.data.file;
    var reader = new FileReader();
    reader.onload = function(progressEvent) {
      // By lines
      var lines = this.result.split("\n");
      storedPoints = {};
      storedKeys = [];
      for (var line = 0; line < lines.length; line++) {
        var comps = lines[line].split(",");
        var x = parseFloat(comps[0]) / 25;
        if (x == undefined || isNaN(x)) continue;
        var y = parseFloat(comps[1]) / 100;
        var z = parseFloat(comps[2]) / 100;
        if (x in storedPoints) {
          storedPoints[x].push(new THREE.Vector3(x, y, z));
        } else {
          storedKeys.push(x);
          storedPoints[x] = [new THREE.Vector3(x, y, z)];
        }
      }
      postMessage({ type: "start" });
    };
    reader.readAsText(file);
  }
});

var fronteir = 0; // track the current verticies.
var lastSection; // track the section that needs to be joined
function iterate() {
  if (storedPoints && storedKeys && globalI < storedKeys.length) {
    var points = [sortPoints(storedPoints[storedKeys[globalI]])];
    var conjoinedPoints = points;
    if (lastSection) {
      conjoinedPoints = [lastSection].concat(points);
    }

    const { vertices, normals } = getVertsAndNormals(conjoinedPoints, 0);
    let uint = new Uint32Array(1);
    let float = new Float32Array(uint.buffer);
    for (let i = 0; i < vertices.length; i++) {
      float[0] = vertices[i];
      Atomics.store(sharedVertArray, fronteir + i, uint[0]);
      float[0] = normals[i];
      Atomics.store(sharedNormArray, fronteir + i, uint[0]);
    }

    fronteir += vertices.length;
    lastSection = points[points.length - 1];
    globalI += 1;
    postMessage({
      verts: fronteir,
      dist: storedKeys[globalI] * 50
    });
  }
}
